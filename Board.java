public class Board{
    private Square[][] tictactoeBoard;
    public Board(){
        tictactoeBoard = new Square[3][3];
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }
    public String toString(){
        String result="  0 1 2 \n";
        for(int row=0;row<3;row++){
            result+=(row +" ");
            for(int col=0;col<3;col++){
                result+=(tictactoeBoard[row][col] + " ");
            }
            result+="\n";
        }
        return result;
    }
    public boolean placeToken(int row, int col, Square playerToken){
        if(row>3 && col>3 || row<0 && col<0){
            return false;
        }
        if(tictactoeBoard[row][col] == Square.BLANK){
            tictactoeBoard[row][col] = playerToken;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean checkIfFull(){
        for(int row=0;row<3;row++){
            for(int col=0;col<3;col++){
                if(tictactoeBoard[row][col]==Square.BLANK){
                    return false;
                }
    }
}
return true;
    }
    private boolean checkIfWinningHorizontal(Square playerToken){
        for(int row=0;row<3;row++){
            if(tictactoeBoard[row][0]==playerToken &&
            tictactoeBoard[row][1]==playerToken &&
            tictactoeBoard[row][2]==playerToken){
            return true;
            }
        }
        return false;
    }
    private boolean checkIfWinningVertical(Square playerToken){
        for(int col=0;col<3;col++){
            if(tictactoeBoard[0][col]==playerToken &&
               tictactoeBoard[1][col]==playerToken &&
               tictactoeBoard[2][col]==playerToken){
                return true;
               }
        }
        return false;
    }
    public boolean checkIfWinning(Square playerToken){
        if(checkIfWinningHorizontal(playerToken)|| checkIfWinningVertical(playerToken)){
            return true;
         }
         return false;
    }    
}