public enum Square{ //represents a single square on a tic-tac-toe board. Possible values --> X,0,BLANK
X("X"),
O("O"),
BLANK("_");

private String symbol;

Square(String symbol){
    this.symbol=symbol;
}
public String toString(){
    return this.symbol;
}
}
