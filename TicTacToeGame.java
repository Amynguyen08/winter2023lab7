import java.util.Scanner;
public class TicTacToeGame{
    Square x = Square.X;
    Square o = Square.O;
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome player!");
        System.out.println("Player 1's token: "+ Square.X);
        System.out.println("Player 2's token: "+ Square.O);
        Board b = new Board();
        //System.out.println(b);
        boolean gameOver = false;
        int player = 1;

        Square playerToken = Square.X;
        while(!gameOver){
            System.out.println(b); 
         if(player==1){
                playerToken = Square.X;
            }
            else{
                playerToken = Square.O;
            }

            System.out.println("Player "+player+" chose a row.");
            int rowChoice = sc.nextInt();
            System.out.println("Chose a column");
            int colChoice = sc.nextInt();
        while(!b.placeToken(rowChoice,colChoice,playerToken)){
            System.out.println("Please enter another position, this one is taken!");
                rowChoice = sc.nextInt();
                colChoice = sc.nextInt();
               // b.placeToken(rowChoice,colChoice,playerToken);
            }
            if(b.checkIfFull()){
                System.out.println("It's a tie!");
                gameOver=true;
            }
            else if(b.checkIfWinning(playerToken)){
                System.out.println(b);
                System.out.println("Player number "player + " is the winner!");
                gameOver=true;
            }
            else{
                player+=1;
                if(player > 2){
                    player = 1;
                }
            }
            }
        }


        
    }
